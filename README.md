# Worldwide Commodity Price Analysis
## By-Aarush Kumar
### Dated: July 23,2021

Worldwide Commodity Price Analysis using Seaborn.
In this project I have used the commodity price dataset from 1980 to 2016 including various commodities such as agriculture, energy, livestock & metal, and then analysing the commodities and visualizing using heatmaps, pairplots.
### The steps involved in the project are as follows:
### 1. Loading the data
![Screenshot_from_2021-07-23_10-01-39](/uploads/571393dd7ff5a6025f97d876bce3ba2e/Screenshot_from_2021-07-23_10-01-39.png)
### 2.Describing the data 
![Screenshot_from_2021-07-23_10-02-05](/uploads/cf28beac884f5c8a4ecf561bdaeecab9/Screenshot_from_2021-07-23_10-02-05.png)
### 3.Visualizing the data
![Screenshot_from_2021-07-23_10-02-27](/uploads/e7a8b2d73263d5ed97748686b8ce1404/Screenshot_from_2021-07-23_10-02-27.png)
![Screenshot_from_2021-07-23_10-02-59](/uploads/8e89eab2774192b230fbcec55c54bbd4/Screenshot_from_2021-07-23_10-02-59.png)
### 4.Heatmaps & pairplots
![Screenshot_from_2021-07-23_10-03-31](/uploads/096b931dfb318f1659ed61629978e23a/Screenshot_from_2021-07-23_10-03-31.png)
![Screenshot_from_2021-07-23_10-03-47](/uploads/edbc09ae101689f6868ff75cbb43d830/Screenshot_from_2021-07-23_10-03-47.png)   
### 5. Conclusions
* Price increase usually happens when there´s product shortage or demand increase, following the supply and demand law. Price devaluation, on the other hand, usually happens when there´s product excess in the market or demand decrease.
* Volatile markets are usually characterized by wide price fluctuations and heavy trading. High volatility often results from an imbalance of trade orders in one direction (for example, all buys and no sells), or it could be due to more speculation, meaning short sellers and institutional investors. It´s a riskier market, with the potential to bring higher profits or losses.
* A positive correlation may indicate the crops share common price-determining factors. Common price determinants for Soybeans and Corn for example are substitutability, demand, biofuels, the value of the U.S. dollar, weather, and crude oil. It´s also important to notice that investors looking to build a well-diversified portfolio will often look to add stocks with such a negative correlation so that as some parts of a portfolio fall in price, others necessarily rise.
