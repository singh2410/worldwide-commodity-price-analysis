#!/usr/bin/env python
# coding: utf-8

# # Commodity Price Worldwide Analysis
# #By- Aarush Kumar
# #Dated: July 21,2021

# In[1]:


get_ipython().system('pip install openpyxl')


# In[2]:


import io
import openpyxl
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import plotly.express as px


# In[3]:


commodity_ds = pd.read_csv("/home/aarush100616/Downloads/Projects/Commodity Price Analysis Worldwide/commodity-prices-2016.csv", sep=",")


# In[4]:


commodity_ds


# In[5]:


#Checking a dataset sample
pd.set_option("display.max_rows", 100)
pd.set_option("display.max_columns", 100)
pd.options.display.float_format="{:,.2f}".format
commodity_ds.sample(n=10, random_state=0)


# In[6]:


#Checking dataset info by feature
commodity_ds.info(verbose=True, null_counts=True)


# In[7]:


#Checking the existence of zeros in rows
(commodity_ds==0).sum(axis=0).to_excel("commodity_ds_zeros_per_feature.xlsx")
(commodity_ds==0).sum(axis=0)


# In[8]:


#Checking the existence of duplicated rows
commodity_ds.duplicated().sum()


# In[9]:


#Checking basic statistical data by feature
describe = commodity_ds.describe(include="all")
std_percentage = pd.DataFrame(describe.iloc[5,:]/describe.iloc[4,:]).T
describe_with_percentage = describe.append(std_percentage)
describe_with_percentage


# ## Data Preparation

# In[10]:


commodity_ds.drop(["All Commodity Price Index", "Non-Fuel Price Index", "Food and Beverage Price Index", "Food Price Index", 
                   "Beverage Price Index", "Industrial Inputs Price Index", "Agricultural Raw Materials Index",
                   "Metals Price Index", "Fuel Energy Index", "Crude Oil petroleum"], axis=1, inplace=True)
commodity_ds["Date"] = pd.to_datetime(commodity_ds["Date"])
commodity_ds["Month/Year"] = commodity_ds["Date"].dt.strftime("%m/%Y")


# ## Data Exploration

# In[11]:


fig = px.line(commodity_ds, x="Month/Year", y=["Bananas", "Coffee Other Mild Arabicas", "Coffee Robusta", "Olive Oil", "Oranges",
                                               "Palm oil", "Soybean Oil", "Sugar European import price", "Sugar Free Market",
                                               "Sugar U.S. import price", "Sunflower oil", "Tea"], height=1000, width=2000)

fig.update_layout(title="Agricultural-Soft Market Prices", xaxis_title="Month/Year", yaxis_title="Price Index", legend_title="Product")


# In[12]:


fig = px.histogram(commodity_ds, x="Month/Year", y=["Bananas", "Coffee Other Mild Arabicas", "Coffee Robusta", "Olive Oil", "Oranges",
                                                    "Palm oil", "Soybean Oil", "Sugar European import price", "Sugar Free Market",
                                                    "Sugar U.S. import price", "Sunflower oil", "Tea"], height=1000, width=2000)
fig.update_layout(title="Agricultural-Soft Market Prices", xaxis_title="Month/Year", yaxis_title="Price Index", legend_title="Product")


# In[13]:


fig = px.line(commodity_ds, x="Month/Year", y=["Barley", "Cocoa beans", "Groundnuts peanuts", "Maize corn", "Rice", "Soybean Meal",
                                               "Soybeans", "Wheat"], height=1000, width=2000)
fig.update_layout(title="Agricultural-Grains Prices", xaxis_title="Month/Year", yaxis_title="Price Index", legend_title="Product")


# In[14]:


fig = px.histogram(commodity_ds, x="Month/Year", y=["Barley", "Cocoa beans", "Groundnuts peanuts", "Maize corn", "Rice", "Soybean Meal",
                                                    "Soybeans", "Wheat"], height=1000, width=2000)
fig.update_layout(title="Agricultural-Grains Prices", xaxis_title="Month/Year", yaxis_title="Price Index", legend_title="Product")


# In[15]:


fig = px.line(commodity_ds, x="Month/Year", y=["Cotton", "Soft Logs", "Hard Logs", "Rubber", "Soft Sawnwood"], 
                                               height=1000, width=2000)
fig.update_layout(title="Agricultural-Not Eatable Prices", xaxis_title="Month/Year", yaxis_title="Price Index", legend_title="Product")


# In[16]:


fig = px.histogram(commodity_ds, x="Month/Year", y=["Cotton", "Soft Logs", "Hard Logs", "Rubber", "Soft Sawnwood"], 
                                                    height=1000, width=2000)
fig.update_layout(title="Agricultural-Not Eatable Prices", xaxis_title="Month/Year", yaxis_title="Price Index", legend_title="Product")


# In[17]:


fig = px.line(commodity_ds, x="Month/Year", y=["Beef", "Fishmeal", "Hides", "Lamb", "Swine - pork", "Poultry chicken",
                                               "Fish salmon", "Hard Sawnwood","Shrimp", "Wool coarse", "Wool fine"],
                                               height=1000, width=2000)
fig.update_layout(title="Livestock Prices", xaxis_title="Month/Year", yaxis_title="Price Index", legend_title="Product")


# In[18]:


fig = px.histogram(commodity_ds, x="Month/Year", y=["Beef", "Fishmeal", "Hides", "Lamb", "Swine - pork", "Poultry chicken",
                                                    "Fish salmon", "Hard Sawnwood","Shrimp", "Wool coarse", "Wool fine"],
                                                    height=1000, width=2000)
fig.update_layout(title="Livestock Prices", xaxis_title="Month/Year", yaxis_title="Price Index", legend_title="Product")


# In[19]:


fig = px.line(commodity_ds, x="Month/Year", y=["Aluminum", "Copper", "China import Iron Ore Fines 62% FE spot", "Lead",
                                               "Nickel", "Tin", "Uranium", "Zinc"], height=1000, width=2000)
fig.update_layout(title="Metal Prices", xaxis_title="Month/Year", yaxis_title="Price Index", legend_title="Product")


# In[20]:


fig = px.histogram(commodity_ds, x="Month/Year", y=["Aluminum", "Copper", "China import Iron Ore Fines 62% FE spot", "Lead",
                                                    "Nickel", "Tin", "Uranium", "Zinc"], height=1000, width=2000)
fig.update_layout(title="Metal Prices", xaxis_title="Month/Year", yaxis_title="Price Index", legend_title="Product")


# In[21]:


fig = px.line(commodity_ds, x="Month/Year", y=["Coal", "Rapeseed oil", "Natural Gas - Russian Natural Gas border price in Germany",
                                               "Natural Gas - Indonesian Liquefied Natural Gas in Japan", "Natural Gas - Spot price at the Henry Hub terminal in Louisiana",
                                               "Crude Oil - petroleum-simple average of three spot prices", "Crude Oil - petroleum - Dated Brent light blend",
                                               "Oil Dubai", "Crude Oil petroleum - West Texas Intermediate 40 API"], height=1000, width=2000)
fig.update_layout(title="Energy Prices", xaxis_title="Month/Year", yaxis_title="Price Index", legend_title="Product")


# In[22]:


fig = px.histogram(commodity_ds, x="Month/Year", y=["Coal", "Rapeseed oil", "Natural Gas - Russian Natural Gas border price in Germany",
                                                    "Natural Gas - Indonesian Liquefied Natural Gas in Japan", "Natural Gas - Spot price at the Henry Hub terminal in Louisiana",
                                                    "Crude Oil - petroleum-simple average of three spot prices", "Crude Oil - petroleum - Dated Brent light blend",
                                                    "Oil Dubai", "Crude Oil petroleum - West Texas Intermediate 40 API"], height=1000, width=2000)
fig.update_layout(title="Energy Prices", xaxis_title="Month/Year", yaxis_title="Price Index", legend_title="Product")


# ## Correlations Analysis

# ### Agriculture

# In[23]:


#Plotting a Heatmap
sns.set(font_scale=1)
fig, ax = plt.subplots(1, figsize=(20,20))
sns.heatmap(commodity_ds[["Bananas", "Coffee Other Mild Arabicas", "Coffee Robusta", "Olive Oil", "Oranges", "Palm oil",
                          "Soybean Oil", "Sugar European import price", "Sugar Free Market", "Sugar U.S. import price",
                          "Sunflower oil", "Tea", "Barley", "Cocoa beans", "Groundnuts peanuts", "Maize corn", "Rice",
                          "Soybean Meal", "Soybeans", "Wheat", "Cotton", "Soft Logs", "Hard Logs", "Rubber",
                          "Soft Sawnwood"]].corr(), annot=True, fmt=",.2f")
plt.title("Agricultural Heatmap Correlation", fontsize=20)
plt.tick_params(labelsize=12)
plt.xticks(rotation=90)
plt.yticks(rotation=45)


# In[32]:


#Printing Sorted Correlation List
commodity_ds[["Bananas", "Coffee Other Mild Arabicas", "Coffee Robusta", "Olive Oil", "Oranges", "Palm oil",
                          "Soybean Oil", "Sugar European import price", "Sugar Free Market", "Sugar U.S. import price",
                          "Sunflower oil", "Tea", "Barley", "Cocoa beans", "Groundnuts peanuts", "Maize corn", "Rice",
                          "Soybean Meal", "Soybeans", "Wheat", "Cotton", "Soft Logs", "Hard Logs", "Rubber",
                          "Soft Sawnwood"]].corr().unstack().sort_values().to_excel("/home/aarush100616/Downloads/Projects/Commodity Price Analysis Worldwide/agricultural_corr.xlsx")


# In[25]:


#Plotting a Pairplot
sns.pairplot(commodity_ds[["Bananas", "Coffee Other Mild Arabicas", "Coffee Robusta", "Olive Oil", "Oranges", "Palm oil",
                           "Soybean Oil", "Sugar European import price", "Sugar Free Market", "Sugar U.S. import price",
                           "Sunflower oil", "Tea", "Barley", "Cocoa beans", "Groundnuts peanuts", "Maize corn", "Rice",
                           "Soybean Meal", "Soybeans", "Wheat", "Cotton", "Soft Logs", "Hard Logs", "Rubber",
                           "Soft Sawnwood"]])


# ### Livestock

# In[26]:


#Plotting a Heatmap
sns.set(font_scale=1)
fig, ax = plt.subplots(1, figsize=(20,20))
sns.heatmap(commodity_ds[["Beef", "Fishmeal", "Hides", "Lamb", "Swine - pork", "Poultry chicken", "Fish salmon", "Hard Sawnwood",
                          "Shrimp", "Wool coarse", "Wool fine"]].corr(), annot=True, fmt=",.2f")
plt.title("Livestock Heatmap Correlation", fontsize=20)
plt.tick_params(labelsize=12)
plt.xticks(rotation=90)
plt.yticks(rotation=45)


# In[31]:


#Printing Sorted Correlation List
commodity_ds[["Beef", "Fishmeal", "Hides", "Lamb", "Swine - pork", "Poultry chicken", "Fish salmon", "Hard Sawnwood",
                          "Shrimp", "Wool coarse", "Wool fine"]].corr().unstack().sort_values().to_excel("/home/aarush100616/Downloads/Projects/Commodity Price Analysis Worldwide/livestock_corr.xlsx")


# In[28]:


#Plotting a Pairplot
sns.pairplot(commodity_ds[["Beef", "Fishmeal", "Hides", "Lamb", "Swine - pork", "Poultry chicken", "Fish salmon", "Hard Sawnwood",
                          "Shrimp", "Wool coarse", "Wool fine"]])


# ### Metal

# In[29]:


#Plotting a Heatmap
sns.set(font_scale=1)
fig, ax = plt.subplots(1, figsize=(20,20))
sns.heatmap(commodity_ds[["Aluminum", "Copper", "China import Iron Ore Fines 62% FE spot", "Lead", "Nickel", "Tin", "Uranium",
                          "Zinc"]].corr(), annot=True, fmt=",.2f")
plt.title("Metal Heatmap Correlation", fontsize=20)
plt.tick_params(labelsize=12)
plt.xticks(rotation=90)
plt.yticks(rotation=45)


# In[30]:


#Printing Sorted Correlation List
commodity_ds[["Aluminum", "Copper", "China import Iron Ore Fines 62% FE spot", "Lead", "Nickel", "Tin", "Uranium",
                          "Zinc"]].corr().unstack().sort_values().to_excel("/home/aarush100616/Downloads/Projects/Commodity Price Analysis Worldwide/metal_corr.xlsx")


# In[33]:


#Plotting a Pairplot
sns.pairplot(commodity_ds[["Aluminum", "Copper", "China import Iron Ore Fines 62% FE spot", "Lead", "Nickel", "Tin", "Uranium",
                           "Zinc"]])


# ### Energy

# In[34]:


#Plotting a Heatmap
sns.set(font_scale=1)
fig, ax = plt.subplots(1, figsize=(20,20))
sns.heatmap(commodity_ds[["Coal", "Rapeseed oil", "Natural Gas - Russian Natural Gas border price in Germany",
                          "Natural Gas - Indonesian Liquefied Natural Gas in Japan", "Natural Gas - Spot price at the Henry Hub terminal in Louisiana",
                          "Crude Oil - petroleum-simple average of three spot prices", "Crude Oil - petroleum - Dated Brent light blend",
                          "Oil Dubai", "Crude Oil petroleum - West Texas Intermediate 40 API"]].corr(), annot=True, fmt=",.2f")
plt.title("Energy Heatmap Correlation", fontsize=20)
plt.tick_params(labelsize=12)
plt.xticks(rotation=90)
plt.yticks(rotation=45)


# In[35]:


#Printing Sorted Correlation List
commodity_ds[["Coal", "Rapeseed oil", "Natural Gas - Russian Natural Gas border price in Germany",
                          "Natural Gas - Indonesian Liquefied Natural Gas in Japan", "Natural Gas - Spot price at the Henry Hub terminal in Louisiana",
                          "Crude Oil - petroleum-simple average of three spot prices", "Crude Oil - petroleum - Dated Brent light blend",
                          "Oil Dubai", "Crude Oil petroleum - West Texas Intermediate 40 API"]].corr().unstack().sort_values().to_excel("/home/aarush100616/Downloads/Projects/Commodity Price Analysis Worldwide/energy_corr.xlsx")


# In[36]:


#Plotting a Pairplot
sns.pairplot(commodity_ds[["Coal", "Rapeseed oil", "Natural Gas - Russian Natural Gas border price in Germany",
                           "Natural Gas - Indonesian Liquefied Natural Gas in Japan", "Natural Gas - Spot price at the Henry Hub terminal in Louisiana",
                           "Crude Oil - petroleum-simple average of three spot prices", "Crude Oil - petroleum - Dated Brent light blend",
                           "Oil Dubai", "Crude Oil petroleum - West Texas Intermediate 40 API"]])


# ## Conclusions

# * Price increase usually happens when there´s product shortage or demand increase, following the supply and demand law. Price devaluation, on the other hand, usually happens when there´s product excess in the market or demand decrease.
# 
# * Volatile markets are usually characterized by wide price fluctuations and heavy trading. High volatility often results from an imbalance of trade orders in one direction (for example, all buys and no sells), or it could be due to more speculation, meaning short sellers and institutional investors. It´s a riskier market, with the potential to bring higher profits or losses.
# 
# * A positive correlation may indicate the crops share common price-determining factors. Common price determinants for Soybeans and Corn for example are substitutability, demand, biofuels, the value of the U.S. dollar, weather, and crude oil. It´s also important to notice that investors looking to build a well-diversified portfolio will often look to add stocks with such a negative correlation so that as some parts of a portfolio fall in price, others necessarily rise.
